# Kubernetes Setup Instructions

Follow these steps to set up your Kubernetes environment:

1. Prepare the Kubernetes environment:

```
make kubernetes/kubespray/prepare
```

2. Create the server:

```
make kubernetes/kubespray/apply
```

3. Create the cluster:

```
make kubernetes/kubespray/install
```

4. Login into cluster:

```
make kubernetes/kubespray/login
```

5. Set default storage class

```
metadata:
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
```

6. Create a Persistent Volume Claim (PVC):

```
make kubernetes/cluster/apply
```

## To destroy the cluster

```
  make kubernetes/kubespray/destroy
```
