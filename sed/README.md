### sed commands

to delete the empty line from the file

```bash
  sed -i '/^[[:space:]]*$/d' [filename]
```
