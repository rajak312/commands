## Commands to merge two projects

```
git remote add source <git-repository>
git fetch origin source
git merge source/main --allow-unrelated-histories

```

fixed the merge conflict if any and then push the code

```
git add .
git commit -m "fixed merge conflict"
git push origin main
```
